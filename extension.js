// Basic stuff
const St = imports.gi.St;
const Lang = imports.lang;

// UI specific components
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const PanelMenu = imports.ui.panelMenu;

const GLib = imports.gi.GLib;
// To run shell command 
const ShellToolkit = imports.gi.St;
const Mainloop = imports.mainloop;
const Clutter = imports.gi.Clutter;

const Me = imports.misc.extensionUtils.getCurrentExtension();
const Gio = imports.gi.Gio;

const Indicator = new Lang.Class({
    Name: 'GazeboNotificationBar', // TODO does this even matter?
    Extends: PanelMenu.Button,

    _init: function() {
        this.parent(0.0, "GazeboNotificationBar", false); // TODO does this even matter?
        global.log("Initializing gazebonotif")
        this.first = true;
        this.enabled = true;
        this.i = 0;
        // this.text = new St.Label({
        //     text: _("Loading..."),
        //     y_align: Clutter.ActorAlign.CENTER
        // });
        this.status_box = new St.BoxLayout();
        gicon = Gio.icon_new_for_string(Me.path + "/icons/gazebo_on.png");
        this.select_icon = new St.Icon({ gicon ,icon_size: 20 });
        this.select_icon.set_gicon(gicon);
        this.status_box.add(this.select_icon);
        this.actor.add_actor(this.status_box);
        let kill_submenu = new PopupMenu.PopupMenuItem('Kill');
        // item.menu.addMenuItem(kill_submenu);
        kill_submenu.actor.connect('button_press_event', Lang.bind(this, function(){
                        this.kill_gazebo();
                    }, false));
        this.menu.addMenuItem(kill_submenu);
        let open_submenu = new PopupMenu.PopupMenuItem('Open');
        // item.menu.addMenuItem(kill_submenu);
        open_submenu.actor.connect('button_press_event', Lang.bind(this, function(){
                        this.open_gzclient();
                    }, false));
        this.menu.addMenuItem(open_submenu);
        this._refresh();
    },

    open_gzclient: function()
    {
        var cmd =  '/bin/bash -c "gzclient"';
        GLib.spawn_command_line_async(cmd)[1];
    },

    kill_gazebo: function()
    {
        var cmd = '/bin/bash -c "killall gzserver; killall gzclient; killall gazebo"';
        GLib.spawn_command_line_sync(cmd)[1];
    },

    _is_gazebo_running: function ()
    {
        // var command_output_bytes = GLib.spawn_command_line_sync('bash -c "source $HOME/.bashrc; /bin/echo $ROS_MASTER_URI"')[1];
        var cmd = '/bin/bash -c "pgrep gzserver"' 
        var command_output_bytes = GLib.spawn_command_line_sync(cmd)[1];
        var command_output_string = '';

        for (var current_character_index = 0;
            current_character_index < command_output_bytes.length;
            ++current_character_index)
        {
            var current_character = String.fromCharCode(command_output_bytes[current_character_index]);
            command_output_string += current_character;
        }
        
        if(this.first == false)
        {
            if (command_output_string != '')
            {
                global.log("gazebo openned")
                
                if (this.enabled == false)
                {
                    show(true)
                    this.enabled = true;
                }
            }
            else
            {
                global.log("gazebo closed")
                if (this.enabled == true )
                {
                    global.log("disabling")
                    this.enabled = false
                    show(false)
                }
            }
        }
        else
        {
            this.first = false
        }

        
    },

    _refresh: function () {
        this._is_gazebo_running();
        this._removeTimeout();
        this._timeout = Mainloop.timeout_add_seconds(2, Lang.bind(this, this._refresh));
        return true;
    },

    _removeTimeout: function () {
        if (this._timeout) {
            Mainloop.source_remove(this._timeout);
            this._timeout = null;
        }
    }

});

let menu;

function init() {  
    
}

function enable() {
    menu = new Indicator();
    Main.panel.addToStatusArea('gazebo_indicator', menu);
    
}

function show(show) {
    Main.panel.statusArea["gazebo_indicator"].actor.visible = show;
}

function disable() {
    menu.destroy();
}